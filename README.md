# ORKG NLP

A repository containing all NLP resources related to the Open Research Knowledge Graph project and related publications

## Note

This repository uses `git lfs` since it contains large files.
Please consider a free space on your local machine before cloning it.