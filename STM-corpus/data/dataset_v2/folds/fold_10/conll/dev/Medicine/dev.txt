-DOCSTART- (S2213158213001253)

In	0,2	O	O
this	3,7	O	O
research	8,16	O	O
,	16,17	O	O
we	18,20	O	O
developed	21,30	O	O
a	31,32	O	B-Method
robust	33,39	O	I-Method
two	40,43	O	I-Method
-	43,44	O	I-Method
layer	44,49	O	I-Method
classifier	50,60	O	I-Method
that	61,65	O	O
can	66,69	O	O
accurately	70,80	O	B-Process
classify	81,89	O	I-Process
normal	90,96	O	B-Material
hearing	97,104	O	I-Material
(	105,106	O	I-Material
NH	106,108	O	I-Material
)	108,109	O	I-Material
from	110,114	O	I-Material
hearing	115,122	O	I-Material
impaired	123,131	O	I-Material
(	132,133	O	I-Material
HI	133,135	O	I-Material
)	135,136	O	I-Material
infants	137,144	O	I-Material
with	145,149	O	I-Material
congenital	150,160	O	I-Material
sensori	161,168	O	I-Material
-	168,169	O	I-Material
neural	169,175	O	I-Material
hearing	176,183	O	I-Material
loss	184,188	O	I-Material
(	189,190	O	I-Material
SNHL	190,194	O	I-Material
)	194,195	O	I-Material
based	196,201	O	O
on	202,204	O	O
their	205,210	O	B-Data
Magnetic	211,219	O	I-Data
Resonance	220,229	O	I-Data
(	230,231	O	I-Data
MR	231,233	O	I-Data
)	233,234	O	I-Data
images	235,241	O	I-Data
.	241,242	O	O

Unlike	243,249	O	O
traditional	250,261	O	O
methods	262,269	O	O
that	270,274	O	O
examine	275,282	O	O
the	283,286	O	B-Data
intensity	287,296	O	I-Data
of	297,299	O	I-Data
each	300,304	O	I-Data
single	305,311	O	I-Data
voxel	312,317	O	I-Data
,	317,318	O	O
we	319,321	O	O
extracted	322,331	O	O
high	332,336	O	B-Data
-	336,337	O	I-Data
level	337,342	O	I-Data
features	343,351	O	I-Data
to	352,354	O	O
characterize	355,367	O	O
the	368,371	O	B-Data
structural	372,382	O	I-Data
MR	383,385	O	I-Data
images	386,392	O	I-Data
(	393,394	O	I-Data
sMRI	394,398	O	I-Data
)	398,399	O	I-Data
and	400,403	O	I-Data
functional	404,414	O	I-Data
MR	415,417	O	I-Data
images	418,424	O	I-Data
(	425,426	O	I-Data
fMRI	426,430	O	I-Data
)	430,431	O	I-Data
.	431,432	O	O

The	433,436	O	B-Method
Scale	437,442	O	I-Method
Invariant	443,452	O	I-Method
Feature	453,460	O	I-Method
Transform	461,470	O	I-Method
(	471,472	O	I-Method
SIFT	472,476	O	I-Method
)	476,477	O	I-Method
algorithm	478,487	O	I-Method
was	488,491	O	O
employed	492,500	O	O
to	501,503	O	O
detect	504,510	O	B-Process
and	511,514	O	O
describe	515,523	O	B-Process
the	524,527	O	B-Data
local	528,533	O	I-Data
features	534,542	O	I-Data
in	543,545	O	O
sMRI	546,550	O	B-Material
.	550,551	O	O

For	552,555	O	O
fMRI	556,560	O	B-Material
,	560,561	O	O
we	562,564	O	O
constructed	565,576	O	B-Process
contrast	577,585	O	B-Material
maps	586,590	O	I-Material
and	591,594	O	O
detected	595,603	O	B-Process
the	604,607	O	B-Material
most	608,612	O	I-Material
activated	613,622	O	I-Material
/	622,623	O	I-Material
de	623,625	O	I-Material
-	625,626	O	I-Material
activated	626,635	O	I-Material
regions	636,643	O	I-Material
in	644,646	O	O
each	647,651	O	B-Material
individual	652,662	O	I-Material
.	662,663	O	O

Based	664,669	O	O
on	670,672	O	O
those	673,678	O	B-Material
salient	679,686	O	I-Material
regions	687,694	O	I-Material
occurring	695,704	O	O
across	705,711	O	O
individuals	712,723	O	B-Material
,	723,724	O	O
the	725,728	O	B-Method
bag	729,732	O	I-Method
-	732,733	O	I-Method
of	733,735	O	I-Method
-	735,736	O	I-Method
words	736,741	O	I-Method
strategy	742,750	O	I-Method
was	751,754	O	O
introduced	755,765	O	O
to	766,768	O	O
vectorize	769,778	O	B-Process
the	779,782	O	B-Material
contrast	783,791	O	I-Material
maps	792,796	O	I-Material
.	796,797	O	O

We	798,800	O	O
then	801,805	O	O
used	806,810	O	O
a	811,812	O	B-Method
two	813,816	O	I-Method
-	816,817	O	I-Method
layer	817,822	O	I-Method
model	823,828	O	I-Method
to	829,831	O	O
integrate	832,841	O	B-Process
these	842,847	O	B-Data
two	848,851	O	I-Data
types	852,857	O	I-Data
of	858,860	O	I-Data
features	861,869	O	I-Data
together	870,878	O	O
.	878,879	O	O

With	880,884	O	O
the	885,888	O	B-Method
leave	889,894	O	I-Method
-	894,895	O	I-Method
one	895,898	O	I-Method
-	898,899	O	I-Method
out	899,902	O	I-Method
cross	903,908	O	I-Method
-	908,909	O	I-Method
validation	909,919	O	I-Method
approach	920,928	O	I-Method
,	928,929	O	O
this	930,934	O	B-Method
integrated	935,945	O	I-Method
model	946,951	O	I-Method
achieved	952,960	O	O
an	961,963	O	B-Data
AUC	964,967	O	I-Data
score	968,973	O	I-Data
of	974,976	O	I-Data
0.90	977,981	O	I-Data
.	981,982	O	O

Additionally	983,995	O	O
,	995,996	O	O
our	997,1000	O	B-Method
algorithm	1001,1010	O	I-Method
highlighted	1011,1022	O	O
several	1023,1030	O	B-Material
important	1031,1040	O	I-Material
brain	1041,1046	O	I-Material
regions	1047,1054	O	I-Material
that	1055,1059	O	O
differentiated	1060,1074	O	B-Process
between	1075,1082	O	B-Material
NH	1083,1085	O	I-Material
and	1086,1089	O	I-Material
HI	1090,1092	O	I-Material
children	1093,1101	O	I-Material
.	1101,1102	O	O

Some	1103,1107	O	O
of	1108,1110	O	O
these	1111,1116	O	B-Material
regions	1117,1124	O	I-Material
,	1124,1125	O	O
e.g.	1126,1130	O	O
planum	1131,1137	O	B-Material
temporale	1138,1147	O	I-Material
and	1148,1151	O	O
angular	1152,1159	O	B-Material
gyrus	1160,1165	O	I-Material
,	1165,1166	O	O
were	1167,1171	O	O
well	1172,1176	O	B-Material
known	1177,1182	O	I-Material
auditory	1183,1191	O	I-Material
and	1192,1195	O	I-Material
visual	1196,1202	O	I-Material
language	1203,1211	O	I-Material
association	1212,1223	O	I-Material
regions	1224,1231	O	I-Material
.	1231,1232	O	O

Others	1233,1239	O	O
,	1239,1240	O	O
e.g.	1241,1245	O	O
the	1246,1249	O	B-Material
anterior	1250,1258	O	I-Material
cingulate	1259,1268	O	I-Material
cortex	1269,1275	O	I-Material
(	1276,1277	O	I-Material
ACC	1277,1280	O	I-Material
)	1280,1281	O	I-Material
,	1281,1282	O	O
were	1283,1287	O	O
not	1288,1291	O	O
necessarily	1292,1303	O	O
expected	1304,1312	O	O
to	1313,1315	O	O
play	1316,1320	O	O
a	1321,1322	O	O
role	1323,1327	O	O
in	1328,1330	O	O
differentiating	1331,1346	O	B-Process
HI	1347,1349	O	B-Material
from	1350,1354	O	I-Material
NH	1355,1357	O	I-Material
children	1358,1366	O	I-Material
and	1367,1370	O	O
provided	1371,1379	O	O
a	1380,1381	O	O
new	1382,1385	O	O
understanding	1386,1399	O	O
of	1400,1402	O	O
brain	1403,1408	O	B-Process
function	1409,1417	O	I-Process
and	1418,1421	O	O
of	1422,1424	O	O
the	1425,1428	O	B-Process
disorder	1429,1437	O	I-Process
itself	1438,1444	O	O
.	1444,1445	O	O

These	1446,1451	O	B-Material
important	1452,1461	O	I-Material
brain	1462,1467	O	I-Material
regions	1468,1475	O	I-Material
provided	1476,1484	O	O
clues	1485,1490	O	B-Data
about	1491,1496	O	O
neuroimaging	1497,1509	O	B-Process
markers	1510,1517	O	B-Data
that	1518,1522	O	O
may	1523,1526	O	O
be	1527,1529	O	O
relevant	1530,1538	O	O
to	1539,1541	O	O
the	1542,1545	O	O
future	1546,1552	O	O
use	1553,1556	O	O
of	1557,1559	O	O
functional	1560,1570	O	B-Process
neuroimaging	1571,1583	O	I-Process
to	1584,1586	O	O
guide	1587,1592	O	O
predictions	1593,1604	O	B-Data
about	1605,1610	O	O
speech	1611,1617	O	B-Process
and	1618,1621	O	I-Process
language	1622,1630	O	I-Process
outcomes	1631,1639	O	I-Process
in	1640,1642	O	O
HI	1643,1645	O	B-Material
infants	1646,1653	O	I-Material
who	1654,1657	O	I-Material
receive	1658,1665	O	I-Material
a	1666,1667	O	I-Material
cochlear	1668,1676	O	I-Material
implant	1677,1684	O	I-Material
.	1684,1685	O	O

This	1686,1690	O	O
type	1691,1695	O	O
of	1696,1698	O	O
prognostic	1699,1709	O	B-Data
information	1710,1721	O	I-Data
could	1722,1727	O	O
be	1728,1730	O	O
extremely	1731,1740	O	O
useful	1741,1747	O	O
and	1748,1751	O	O
is	1752,1754	O	O
currently	1755,1764	O	O
not	1765,1768	O	O
available	1769,1778	O	O
to	1779,1781	O	O
clinicians	1782,1792	O	O
by	1793,1795	O	O
any	1796,1799	O	O
other	1800,1805	O	O
means	1806,1811	O	O
.	1811,1812	O	O

Highlights	1813,1823	O	O
•	1824,1825	O	O

We	1826,1828	O	O
probe	1829,1834	O	B-Process
brain	1835,1840	O	B-Process
structural	1841,1851	O	I-Process
and	1852,1855	O	I-Process
functional	1856,1866	O	I-Process
changes	1867,1874	O	I-Process
in	1875,1877	O	O
hearing	1878,1885	O	B-Material
impaired	1886,1894	O	I-Material
(	1895,1896	O	I-Material
HI	1896,1898	O	I-Material
)	1898,1899	O	I-Material
infants	1900,1907	O	I-Material
.	1907,1908	O	O

•	1909,1910	O	O

We	1911,1913	O	O
build	1914,1919	O	O
a	1920,1921	O	B-Method
robust	1922,1928	O	I-Method
two	1929,1932	O	I-Method
-	1932,1933	O	I-Method
layer	1933,1938	O	I-Method
classifier	1939,1949	O	I-Method
that	1950,1954	O	O
integrates	1955,1965	O	B-Process
sMRI	1966,1970	O	B-Material
and	1971,1974	O	I-Material
fMRI	1975,1979	O	I-Material
data	1980,1984	O	I-Material
.	1984,1985	O	O

•	1986,1987	O	O

This	1988,1992	O	B-Method
integrated	1993,2003	O	I-Method
model	2004,2009	O	I-Method
accurately	2010,2020	O	B-Process
separates	2021,2030	O	I-Process
HI	2031,2033	O	B-Material
from	2034,2038	O	I-Material
normal	2039,2045	O	I-Material
infants	2046,2053	O	I-Material
(	2054,2055	O	O
AUC	2055,2058	O	B-Data
0.9	2059,2062	O	I-Data
)	2062,2063	O	O
.	2063,2064	O	O

•	2065,2066	O	O

Our	2067,2070	O	B-Method
method	2071,2077	O	I-Method
detects	2078,2085	O	O
important	2086,2095	O	B-Material
brain	2096,2101	O	I-Material
regions	2102,2109	O	I-Material
different	2110,2119	O	O
between	2120,2127	O	O
HI	2128,2130	O	B-Material
and	2131,2134	O	I-Material
normal	2135,2141	O	I-Material
infants	2142,2149	O	I-Material
.	2149,2150	O	O

•	2151,2152	O	O

Our	2153,2156	O	B-Method
method	2157,2163	O	I-Method
can	2164,2167	O	O
include	2168,2175	O	O
diverse	2176,2183	O	B-Material
types	2184,2189	O	I-Material
of	2190,2192	O	I-Material
data	2193,2197	O	I-Material
and	2198,2201	O	O
be	2202,2204	O	O
applied	2205,2212	O	O
to	2213,2215	O	O
other	2216,2221	O	O
diseases	2222,2230	O	B-Process
.	2230,2231	O	O

