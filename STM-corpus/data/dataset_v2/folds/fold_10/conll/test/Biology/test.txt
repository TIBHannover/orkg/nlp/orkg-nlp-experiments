-DOCSTART- (S016816561300552X)

The	0,3	O	B-Process
transfer	4,12	O	I-Process
of	13,15	O	O
a	16,17	O	B-Process
laboratory	18,28	O	I-Process
process	29,36	O	I-Process
into	37,41	O	O
a	42,43	O	B-Material
manufacturing	44,57	O	I-Material
facility	58,66	O	I-Material
is	67,69	O	O
one	70,73	O	O
of	74,76	O	O
the	77,80	O	O
most	81,85	O	O
critical	86,94	O	O
steps	95,100	O	O
required	101,109	O	O
for	110,113	O	O
the	114,117	O	B-Process
large	118,123	O	I-Process
scale	124,129	O	I-Process
production	130,140	O	I-Process
of	141,143	O	O
cell	144,148	O	B-Material
-	148,149	O	I-Material
based	149,154	O	I-Material
therapy	155,162	O	I-Material
products	163,171	O	I-Material
.	171,172	O	O

This	173,177	O	O
study	178,183	O	O
describes	184,193	O	O
the	194,197	O	B-Method
first	198,203	O	I-Method
published	204,213	O	I-Method
protocol	214,222	O	I-Method
for	223,226	O	O
scalable	227,235	O	B-Process
automated	236,245	O	I-Process
expansion	246,255	O	I-Process
of	256,258	O	O
human	259,264	O	B-Material
induced	265,272	O	I-Material
pluripotent	273,284	O	I-Material
stem	285,289	O	I-Material
cell	290,294	O	I-Material
lines	295,300	O	I-Material
growing	301,308	O	B-Process
in	309,311	O	O
aggregates	312,322	O	B-Material
in	323,325	O	O
feeder	326,332	O	B-Material
-	332,333	O	I-Material
free	333,337	O	I-Material
and	338,341	O	I-Material
chemically	342,352	O	I-Material
defined	353,360	O	I-Material
medium	361,367	O	I-Material
.	367,368	O	O

Cells	369,374	O	B-Material
were	375,379	O	O
successfully	380,392	O	O
transferred	393,404	O	B-Process
between	405,412	O	O
different	413,422	O	B-Material
sites	423,428	O	I-Material
representative	429,443	O	O
of	444,446	O	O
research	447,455	O	B-Data
and	456,459	O	I-Data
manufacturing	460,473	O	I-Data
settings	474,482	O	I-Data
;	482,483	O	O
and	484,487	O	O
passaged	488,496	O	B-Process
manually	497,505	O	I-Process
and	506,509	O	O
using	510,515	O	O
the	516,519	O	B-Material
CompacT	520,527	O	I-Material
SelecT	528,534	O	I-Material
automation	535,545	O	I-Material
platform	546,554	O	I-Material
.	554,555	O	O

Modified	556,564	O	B-Process
protocols	565,574	O	I-Process
were	575,579	O	O
developed	580,589	O	O
for	590,593	O	O
the	594,597	O	B-Material
automated	598,607	O	I-Material
system	608,614	O	I-Material
and	615,618	O	O
the	619,622	O	B-Process
management	623,633	O	I-Process
of	634,636	O	O
cells	637,642	O	B-Material
aggregates	643,653	O	I-Material
(	654,655	O	I-Material
clumps	655,661	O	I-Material
)	661,662	O	I-Material
was	663,666	O	O
identified	667,677	O	O
as	678,680	O	O
the	681,684	O	O
critical	685,693	O	O
step	694,698	O	O
.	698,699	O	O

Cellular	700,708	O	B-Data
morphology	709,719	O	I-Data
,	719,720	O	O
pluripotency	721,733	O	B-Process
gene	734,738	O	I-Process
expression	739,749	O	I-Process
and	750,753	O	O
differentiation	754,769	O	B-Process
into	770,774	O	O
the	775,778	O	B-Material
three	779,784	O	I-Material
germ	785,789	O	I-Material
layers	790,796	O	I-Material
have	797,801	O	O
been	802,806	O	O
used	807,811	O	O
compare	812,819	O	B-Process
the	820,823	O	B-Data
outcomes	824,832	O	I-Data
of	833,835	O	O
manual	836,842	O	B-Process
and	843,846	O	I-Process
automated	847,856	O	I-Process
processes	857,866	O	I-Process
.	866,867	O	O

-DOCSTART- (S2213671113001288)

Whether	0,7	O	O
human	8,13	O	B-Material
induced	14,21	O	I-Material
pluripotent	22,33	O	I-Material
stem	34,38	O	I-Material
cells	39,44	O	I-Material
(	45,46	O	I-Material
hiPSCs	46,52	O	I-Material
)	52,53	O	I-Material
are	54,57	O	O
epigenetically	58,72	O	B-Process
identical	73,82	O	O
to	83,85	O	O
human	86,91	O	B-Material
embryonic	92,101	O	I-Material
stem	102,106	O	I-Material
cells	107,112	O	I-Material
(	113,114	O	I-Material
hESCs	114,119	O	I-Material
)	119,120	O	I-Material
has	121,124	O	O
been	125,129	O	O
debated	130,137	O	O
in	138,140	O	O
the	141,144	O	B-Process
stem	145,149	O	I-Process
cell	150,154	O	I-Process
field	155,160	O	I-Process
.	160,161	O	O

In	162,164	O	O
this	165,169	O	O
study	170,175	O	O
,	175,176	O	O
we	177,179	O	O
analyzed	180,188	O	B-Process
DNA	189,192	O	B-Data
methylation	193,204	O	I-Data
patterns	205,213	O	I-Data
in	214,216	O	O
a	217,218	O	O
large	219,224	O	O
number	225,231	O	O
of	232,234	O	O
hiPSCs	235,241	O	B-Material
(	242,243	O	O
n	243,244	O	B-Data
=	245,246	O	I-Data
114	247,250	O	I-Data
)	250,251	O	O
and	252,255	O	O
hESCs	256,261	O	B-Material
(	262,263	O	O
n	263,264	O	B-Data
=	265,266	O	I-Data
155	267,270	O	I-Data
)	270,271	O	O
,	271,272	O	O
and	273,276	O	O
identified	277,287	O	B-Process
a	288,289	O	B-Material
panel	290,295	O	I-Material
of	296,298	O	O
82	299,301	O	B-Data
CpG	302,305	O	B-Material
methylation	306,317	O	I-Material
sites	318,323	O	I-Material
that	324,328	O	O
can	329,332	O	O
distinguish	333,344	O	B-Process
hiPSCs	345,351	O	B-Material
from	352,356	O	O
hESCs	357,362	O	B-Material
with	363,367	O	O
high	368,372	O	B-Data
accuracy	373,381	O	I-Data
.	381,382	O	O

We	383,385	O	O
show	386,390	O	O
that	391,395	O	O
12	396,398	O	B-Data
out	399,402	O	I-Data
of	403,405	O	I-Data
the	406,409	O	I-Data
82	410,412	O	I-Data
CpG	413,416	O	B-Material
sites	417,422	O	I-Material
were	423,427	O	O
subject	428,435	O	O
to	436,438	O	O
hypermethylation	439,455	O	B-Process
in	456,458	O	O
part	459,463	O	O
by	464,466	O	O

DNMT3B.	467,474	O	B-Material
Notably	475,482	O	O
,	482,483	O	O
DNMT3B	484,490	O	B-Material
contributes	491,502	O	O
directly	503,511	O	O
to	512,514	O	O
aberrant	515,523	O	B-Process
hypermethylation	524,540	O	I-Process
and	541,544	O	O
silencing	545,554	O	B-Process
of	555,557	O	O
the	558,561	O	B-Material
signature	562,571	O	I-Material
gene	572,576	O	I-Material
,	576,577	O	I-Material

TCERG1L.	578,586	O	I-Material
Overall	587,594	O	O
,	594,595	O	O
we	596,598	O	O
conclude	599,607	O	O
that	608,612	O	O
DNMT3B	613,619	O	B-Material
is	620,622	O	O
involved	623,631	O	O
in	632,634	O	O
a	635,636	O	B-Process
wave	637,641	O	I-Process
of	642,644	O	I-Process
de	645,647	O	I-Process
novo	648,652	O	I-Process
methylation	653,664	O	I-Process
during	665,671	O	O
reprogramming	672,685	O	B-Process
,	685,686	O	O
a	687,688	O	O
portion	689,696	O	O
of	697,699	O	O
which	700,705	O	O
contributes	706,717	O	O
to	718,720	O	O
the	721,724	O	B-Data
unique	725,731	O	I-Data
hiPSC	732,737	O	I-Data
methylation	738,749	O	I-Data
signature	750,759	O	I-Data
.	759,760	O	O

These	761,766	O	O
82	767,769	O	B-Data
CpG	770,773	O	B-Material
methylation	774,785	O	I-Material
sites	786,791	O	I-Material
may	792,795	O	O
be	796,798	O	O
useful	799,805	O	O
as	806,808	O	O
biomarkers	809,819	O	B-Material
to	820,822	O	O
distinguish	823,834	O	B-Process
between	835,842	O	O
hiPSCs	843,849	O	B-Material
and	850,853	O	O
hESCs	854,859	O	B-Material
.	859,860	O	O

Highlights	861,871	O	O
•	872,873	O	O

Eighty	874,880	O	B-Data
-	880,881	O	I-Data
two	881,884	O	I-Data
DNA	885,888	O	B-Material
methylation	889,900	O	I-Material
sites	901,906	O	I-Material
distinguish	907,918	O	B-Process
hiPSCs	919,925	O	B-Material
and	926,929	O	O
hESCs	930,935	O	B-Material
with	936,940	O	O
high	941,945	O	B-Data
accuracy	946,954	O	I-Data
•	955,956	O	O

Twelve	957,963	O	B-Data
aberrantly	964,974	O	B-Material
hypermethylated	975,990	O	I-Material
signature	991,1000	O	I-Material
CG	1001,1003	O	I-Material
sites	1004,1009	O	I-Material
are	1010,1013	O	O
due	1014,1017	O	O
in	1018,1020	O	O
part	1021,1025	O	O
to	1026,1028	O	O
DNMT3B	1029,1035	O	B-Material
•	1036,1037	O	O
DNMT3B	1038,1044	O	B-Material
also	1045,1049	O	O
contributes	1050,1061	O	O
to	1062,1064	O	O
previously	1065,1075	O	O
reported	1076,1084	O	O
aberrant	1085,1093	O	B-Process
hypermethylation	1094,1110	O	I-Process
in	1111,1113	O	O
hiPSCs	1114,1120	O	B-Material

Huang	1121,1126	O	O
et	1127,1129	O	O
al	1130,1132	O	O
.	1132,1133	O	O
identify	1134,1142	O	O
a	1143,1144	O	B-Material
panel	1145,1150	O	I-Material
of	1151,1153	O	O
82	1154,1156	O	B-Data
CpG	1157,1160	O	B-Material
methylation	1161,1172	O	I-Material
sites	1173,1178	O	I-Material
that	1179,1183	O	O
can	1184,1187	O	O
robustly	1188,1196	O	O
distinguish	1197,1208	O	B-Process
hiPSCs	1209,1215	O	B-Material
from	1216,1220	O	O
hESCs	1221,1226	O	B-Material
with	1227,1231	O	O
high	1232,1236	O	B-Data
accuracy	1237,1245	O	I-Data
.	1245,1246	O	O

Part	1247,1251	O	O
of	1252,1254	O	O
the	1255,1258	O	B-Data
signature	1259,1268	O	I-Data
consists	1269,1277	O	O
of	1278,1280	O	O
aberrantly	1281,1291	O	B-Material
hypermethylated	1292,1307	O	I-Material
CpG	1308,1311	O	I-Material
sites	1312,1317	O	I-Material
,	1317,1318	O	O
which	1319,1324	O	O
are	1325,1328	O	O
partially	1329,1338	O	B-Process
abrogated	1339,1348	O	I-Process
in	1349,1351	O	O
the	1352,1355	O	O
absence	1356,1363	O	O
of	1364,1366	O	O
DNMT3B.	1367,1374	O	B-Material

In	1375,1377	O	O
addition	1378,1386	O	O
,	1386,1387	O	O
DNMT3B	1388,1394	O	B-Material
-	1394,1395	O	I-Material
mutant	1395,1401	O	I-Material
hiPSCs	1402,1408	O	I-Material
do	1409,1411	O	O
not	1412,1415	O	O
exhibit	1416,1423	O	O
other	1424,1429	O	B-Data
aberrant	1430,1438	O	I-Data
hypermethylation	1439,1455	O	I-Data
signatures	1456,1466	O	I-Data
as	1467,1469	O	O
previously	1470,1480	O	O
reported	1481,1489	O	O
.	1489,1490	O	O

