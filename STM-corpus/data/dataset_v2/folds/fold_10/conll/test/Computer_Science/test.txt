-DOCSTART- (S0167739X12001525)

To	0,2	O	O
guarantee	3,12	O	O
the	13,16	O	O
vision	17,23	O	O
of	24,26	O	O
Cloud	27,32	O	B-Process
Computing	33,42	O	I-Process
QoS	43,46	O	I-Process
goals	47,52	O	O
between	53,60	O	O
the	61,64	O	B-Material
Cloud	65,70	O	I-Material
provider	71,79	O	O
and	80,83	O	O
the	84,87	O	O
customer	88,96	O	O
have	97,101	O	O
to	102,104	O	O
be	105,107	O	O
dynamically	108,119	O	O
met	120,123	O	O
.	123,124	O	O

This	125,129	O	O
so	130,132	O	O
-	132,133	O	O
called	133,139	O	O
Service	140,147	O	B-Process
Level	148,153	O	I-Process
Agreement	154,163	O	I-Process
(	164,165	O	I-Process
SLA	165,168	O	I-Process
)	168,169	O	I-Process
enactment	170,179	O	I-Process
should	180,186	O	O
involve	187,194	O	O
little	195,201	O	O
human	202,207	O	B-Process
-	207,208	O	I-Process
based	208,213	O	I-Process
interaction	214,225	O	I-Process
in	226,228	O	O
order	229,234	O	O
to	235,237	O	O
guarantee	238,247	O	O
the	248,251	O	B-Process
scalability	252,263	O	I-Process
and	264,267	O	I-Process
efficient	268,277	O	I-Process
resource	278,286	O	I-Process
utilization	287,298	O	I-Process
of	299,301	O	O
the	302,305	O	B-Material
system	306,312	O	I-Material
.	312,313	O	O

To	314,316	O	O
achieve	317,324	O	O
this	325,329	O	O
we	330,332	O	O
start	333,338	O	O
from	339,343	O	O
Autonomic	344,353	O	B-Process
Computing	354,363	O	I-Process
,	363,364	O	O
examine	365,372	O	O
the	373,376	O	B-Process
autonomic	377,386	O	I-Process
control	387,394	O	I-Process
loop	395,399	O	I-Process
and	400,403	O	O
adapt	404,409	O	O
it	410,412	O	O
to	413,415	O	O
govern	416,422	O	O
Cloud	423,428	O	B-Material
Computing	429,438	O	I-Material
infrastructures	439,454	O	I-Material
.	454,455	O	O

We	456,458	O	O
first	459,464	O	O
hierarchically	465,479	O	B-Process
structure	480,489	O	I-Process
all	490,493	O	B-Process
possible	494,502	O	I-Process
adaptation	503,513	O	I-Process
actions	514,521	O	I-Process
into	522,526	O	O
so	527,529	O	B-Data
-	529,530	O	I-Data
called	530,536	O	I-Data
escalation	537,547	O	I-Data
levels	548,554	O	I-Data
.	554,555	O	O

We	556,558	O	O
then	559,563	O	O
focus	564,569	O	O
on	570,572	O	O
one	573,576	O	B-Data
of	577,579	O	I-Data
these	580,585	O	I-Data
levels	586,592	O	I-Data
by	593,595	O	O
analyzing	596,605	O	B-Process
monitored	606,615	O	B-Data
data	616,620	O	I-Data
from	621,625	O	O
virtual	626,633	O	B-Material
machines	634,642	O	I-Material
and	643,646	O	O
making	647,653	O	O
decisions	654,663	O	O
on	664,666	O	O
their	667,672	O	B-Data
resource	673,681	O	I-Data
configuration	682,695	O	I-Data
with	696,700	O	O
the	701,704	O	O
help	705,709	O	O
of	710,712	O	O
knowledge	713,722	O	B-Process
management	723,733	O	I-Process
(	734,735	O	I-Process
KM	735,737	O	I-Process
)	737,738	O	I-Process
.	738,739	O	O

The	740,743	O	B-Data
monitored	744,753	O	I-Data
data	754,758	O	I-Data
stems	759,764	O	O
both	765,769	O	O
from	770,774	O	O
synthetically	775,788	O	B-Data
generated	789,798	O	I-Data
workload	799,807	O	I-Data
categorized	808,819	O	B-Process
in	820,822	O	O
different	823,832	O	B-Data
workload	833,841	O	I-Data
volatility	842,852	O	I-Data
classes	853,860	O	I-Data
and	861,864	O	O
from	865,869	O	O
a	870,871	O	B-Process
real	872,876	O	I-Process
-	876,877	O	I-Process
world	877,882	O	I-Process
scenario	883,891	O	I-Process
:	891,892	O	O
scientific	893,903	O	B-Material
workflow	904,912	O	I-Material
applications	913,925	O	I-Material
in	926,928	O	O
bioinformatics	929,943	O	B-Process
.	943,944	O	O

As	945,947	O	O
KM	948,950	O	B-Process
techniques	951,961	O	O
,	961,962	O	O
we	963,965	O	O
investigate	966,977	O	O
two	978,981	O	O
methods	982,989	O	O
,	989,990	O	O
Case	991,995	O	B-Method
-	995,996	O	I-Method
Based	996,1001	O	I-Method
Reasoning	1002,1011	O	I-Method
and	1012,1015	O	I-Method
a	1016,1017	O	I-Method
rule	1018,1022	O	I-Method
-	1022,1023	O	I-Method
based	1023,1028	O	I-Method
approach	1029,1037	O	I-Method
.	1037,1038	O	O

We	1039,1041	O	O
design	1042,1048	O	O
and	1049,1052	O	O
implement	1053,1062	O	O
both	1063,1067	O	B-Method
of	1068,1070	O	I-Method
them	1071,1075	O	I-Method
and	1076,1079	O	O
evaluate	1080,1088	O	B-Process
them	1089,1093	O	O
with	1094,1098	O	O
the	1099,1102	O	O
help	1103,1107	O	O
of	1108,1110	O	O
a	1111,1112	O	B-Material
simulation	1113,1123	O	I-Material
engine	1124,1130	O	I-Material
.	1130,1131	O	O

Simulation	1132,1142	O	B-Material
reveals	1143,1150	O	O
the	1151,1154	O	O
feasibility	1155,1166	O	O
of	1167,1169	O	O
the	1170,1173	O	B-Method
CBR	1174,1177	O	I-Method
approach	1178,1186	O	I-Method
and	1187,1190	O	O
major	1191,1196	O	O
improvements	1197,1209	O	O
by	1210,1212	O	O
the	1213,1216	O	B-Method
rule	1217,1221	O	I-Method
-	1221,1222	O	I-Method
based	1222,1227	O	I-Method
approach	1228,1236	O	I-Method
considering	1237,1248	O	O
SLA	1249,1252	O	B-Process
violations	1253,1263	O	I-Process
,	1263,1264	O	O
resource	1265,1273	O	B-Process
utilization	1274,1285	O	I-Process
,	1285,1286	O	O
the	1287,1290	O	B-Data
number	1291,1297	O	I-Data
of	1298,1300	O	I-Data
necessary	1301,1310	O	I-Data
reconfigurations	1311,1327	O	I-Data
and	1328,1331	O	I-Data
time	1332,1336	O	I-Data
performance	1337,1348	O	I-Data
for	1349,1352	O	O
both	1353,1357	O	O
,	1357,1358	O	O
synthetically	1359,1372	O	B-Data
generated	1373,1382	O	I-Data
and	1383,1386	O	I-Data
real	1387,1391	O	I-Data
-	1391,1392	O	I-Data
world	1392,1397	O	I-Data
data	1398,1402	O	I-Data
.	1402,1403	O	O

Highlights	1404,1414	O	O

►	1415,1416	O	O

We	1417,1419	O	O
apply	1420,1425	O	O
knowledge	1426,1435	O	B-Process
management	1436,1446	O	I-Process
to	1447,1449	O	O
guarantee	1450,1459	O	O
SLAs	1460,1464	O	B-Method
and	1465,1468	O	O
low	1469,1472	O	B-Process
resource	1473,1481	O	I-Process
wastage	1482,1489	O	I-Process
in	1490,1492	O	O
Clouds	1493,1499	O	B-Material
.	1499,1500	O	O

►	1501,1502	O	O

Escalation	1503,1513	O	B-Data
levels	1514,1520	O	I-Data
provide	1521,1528	O	O
a	1529,1530	O	B-Material
hierarchical	1531,1543	O	I-Material
model	1544,1549	O	I-Material
to	1550,1552	O	O
structure	1553,1562	O	B-Process
possible	1563,1571	O	O
reconfiguration	1572,1587	O	B-Process
actions	1588,1595	O	I-Process
.	1595,1596	O	O

►	1597,1598	O	O

Case	1599,1603	O	B-Method
-	1603,1604	O	I-Method
Based	1604,1609	O	I-Method
Reasoning	1610,1619	O	I-Method
and	1620,1623	O	I-Method
rule	1624,1628	O	I-Method
-	1628,1629	O	I-Method
based	1629,1634	O	I-Method
approach	1635,1643	O	I-Method
prove	1644,1649	O	O
feasibility	1650,1661	O	O
as	1662,1664	O	O
KM	1665,1667	O	B-Process
techniques	1668,1678	O	O
.	1678,1679	O	O

►	1680,1681	O	O

In	1682,1684	O	B-Process
-	1684,1685	O	I-Process
depth	1685,1690	O	I-Process
evaluation	1691,1701	O	I-Process
of	1702,1704	O	O
rule	1705,1709	O	B-Method
-	1709,1710	O	I-Method
based	1710,1715	O	I-Method
approach	1716,1724	O	I-Method
shows	1725,1730	O	O
major	1731,1736	O	O
improvements	1737,1749	O	O
towards	1750,1757	O	O
CBR	1758,1761	O	B-Method
.	1761,1762	O	O

►	1763,1764	O	O

KM	1765,1767	O	B-Process
is	1768,1770	O	O
applied	1771,1778	O	O
to	1779,1781	O	O
real	1782,1786	O	B-Data
-	1786,1787	O	I-Data
world	1787,1792	O	I-Data
data	1793,1797	O	I-Data
gathered	1798,1806	O	O
from	1807,1811	O	O
scientific	1812,1822	O	B-Process
bioinformatic	1823,1836	O	I-Process
workflows	1837,1846	O	I-Process
.	1846,1847	O	O

-DOCSTART- (S1084804513001987)

Precomputation	0,14	O	B-Process
of	15,17	O	O
the	18,21	O	B-Process
supported	22,31	O	I-Process
QoS	32,35	O	I-Process
is	36,38	O	O
very	39,43	O	O
important	44,53	O	O
for	54,57	O	O
internet	58,66	O	B-Process
routing	67,74	O	I-Process
.	74,75	O	O

By	76,78	O	O
constructing	79,91	O	B-Process
routing	92,99	O	B-Material
tables	100,106	O	I-Material
before	107,113	O	O
a	114,115	O	B-Data
request	116,123	O	I-Data
arrives	124,131	O	B-Process
,	131,132	O	O
a	133,134	O	B-Data
packet	135,141	O	I-Data
can	142,145	O	O
be	146,148	O	O
forwarded	149,158	O	B-Process
with	159,163	O	O
a	164,165	O	B-Method
simple	166,172	O	I-Method
table	173,178	O	I-Method
lookup	179,185	O	I-Method
.	185,186	O	O

When	187,191	O	O
the	192,195	O	B-Data
QoS	196,199	O	I-Data
information	200,211	O	I-Data
is	212,214	O	O
provided	215,223	O	O
,	223,224	O	O
a	225,226	O	B-Material
node	227,231	O	I-Material
can	232,235	O	O
immediately	236,247	O	O
know	248,252	O	O
whether	253,260	O	O
a	261,262	O	B-Data
certain	263,270	O	I-Data
request	271,278	O	I-Data
can	279,282	O	O
be	283,285	O	O
supported	286,295	O	O
without	296,303	O	O
launching	304,313	O	B-Process
the	314,317	O	B-Process
path	318,322	O	I-Process
finding	323,330	O	I-Process
process	331,338	O	I-Process
.	338,339	O	O

Unfortunately	340,353	O	O
,	353,354	O	O
as	355,357	O	O
the	358,361	O	O
problem	362,369	O	O
of	370,372	O	O
finding	373,380	O	O
a	381,382	O	B-Process
route	383,388	O	I-Process
satisfying	389,399	O	O
two	400,403	O	B-Data
additive	404,412	O	I-Data
constraints	413,424	O	I-Data
is	425,427	O	O
NP	428,430	O	B-Data
-	430,431	O	I-Data
complete	431,439	O	I-Data
,	439,440	O	O
the	441,444	O	B-Data
supported	445,454	O	I-Data
QoS	455,458	O	I-Data
information	459,470	O	I-Data
can	471,474	O	O
only	475,479	O	O
be	480,482	O	O
approximated	483,495	O	B-Process
using	496,501	O	O
a	502,503	O	B-Process
polynomial	504,514	O	I-Process
time	515,519	O	I-Process
mechanism	520,529	O	I-Process
.	529,530	O	O

A	531,532	O	B-Process
good	533,537	O	I-Process
approximation	538,551	O	I-Process
scheme	552,558	O	I-Process
should	559,565	O	O
reduce	566,572	O	B-Process
the	573,576	O	B-Data
error	577,582	O	I-Data
in	583,585	O	O
estimating	586,596	O	B-Process
the	597,600	O	B-Data
actual	601,607	O	I-Data
supported	608,617	O	I-Data
QoS.	618,622	O	I-Data

Nevertheless	623,635	O	O
,	635,636	O	O
existing	637,645	O	O
approaches	646,656	O	O
which	657,662	O	O
determine	663,672	O	O
this	673,677	O	B-Data
error	678,683	O	I-Data
may	684,687	O	O
not	688,691	O	O
truly	692,697	O	O
reflect	698,705	O	O
the	706,709	O	B-Data
performance	710,721	O	I-Data
on	722,724	O	O
admission	725,734	O	B-Process
control	735,742	O	I-Process
,	742,743	O	O
meaning	744,751	O	O
whether	752,759	O	O
a	760,761	O	B-Data
request	762,769	O	I-Data
can	770,773	O	O
be	774,776	O	O
correctly	777,786	O	B-Process
classified	787,797	O	I-Process
as	798,800	O	O
feasible	801,809	O	B-Data
or	810,812	O	I-Data
infeasible	813,823	O	I-Data
.	823,824	O	O

In	825,827	O	O
this	828,832	O	O
paper	833,838	O	O
,	838,839	O	O
we	840,842	O	O
propose	843,850	O	O
using	851,856	O	O
a	857,858	O	B-Method
novel	859,864	O	I-Method
metric	865,871	O	I-Method
,	871,872	O	O
known	873,878	O	O
as	879,881	O	O
distortion	882,892	O	B-Data
area	893,897	O	I-Data
,	897,898	O	O
to	899,901	O	O
evaluate	902,910	O	B-Process
the	911,914	O	B-Data
performance	915,926	O	I-Data
of	927,929	O	O
precomputing	930,942	O	B-Process
the	943,946	O	B-Data
supported	947,956	O	I-Data
QoS.	957,961	O	I-Data
We	962,964	O	O
then	965,969	O	O
analyze	970,977	O	O
the	978,981	O	B-Data
performance	982,993	O	I-Data
of	994,996	O	O
the	997,1000	O	B-Material
class	1001,1006	O	I-Material
of	1007,1009	O	I-Material
algorithms	1010,1020	O	I-Material
that	1021,1025	O	O
approximate	1026,1037	O	B-Process
the	1038,1041	O	B-Data
supported	1042,1051	O	I-Data
QoS	1052,1055	O	I-Data
through	1056,1063	O	O
discretizing	1064,1076	O	B-Process
link	1077,1081	O	B-Data
metrics	1082,1089	O	I-Data
.	1089,1090	O	O

We	1091,1093	O	O
demonstrate	1094,1105	O	O
how	1106,1109	O	O
the	1110,1113	O	B-Data
performance	1114,1125	O	I-Data
of	1126,1128	O	O
these	1129,1134	O	B-Process
schemes	1135,1142	O	I-Process
can	1143,1146	O	O
be	1147,1149	O	O
enhanced	1150,1158	O	O
without	1159,1166	O	O
increasing	1167,1177	O	B-Process
complexity	1178,1188	O	B-Data
.	1188,1189	O	O

Our	1190,1193	O	O
results	1194,1201	O	O
serve	1202,1207	O	O
as	1208,1210	O	O
a	1211,1212	O	O
guideline	1213,1222	O	O
on	1223,1225	O	O
developing	1226,1236	O	O
discretization	1237,1251	O	B-Method
-	1251,1252	O	I-Method
based	1252,1257	O	I-Method
approximation	1258,1271	O	I-Method
algorithms	1272,1282	O	I-Method
.	1282,1283	O	O

