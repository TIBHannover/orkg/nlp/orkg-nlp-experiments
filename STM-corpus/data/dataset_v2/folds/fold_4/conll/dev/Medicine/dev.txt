-DOCSTART- (S0960896612001022)

Over	0,4	O	O
20years	5,12	O	O
ago	13,16	O	O
single	17,23	O	B-Process
clonal	24,30	O	I-Process
deletions	31,40	O	I-Process
were	41,45	O	O
the	46,49	O	B-Process
first	50,55	O	I-Process
mitochondrial	56,69	O	I-Process
DNA	70,73	O	I-Process
(	74,75	O	I-Process
mtDNA	75,80	O	I-Process
)	80,81	O	I-Process
genetic	82,89	O	I-Process
defects	90,97	O	I-Process
described	98,107	O	O
in	108,110	O	O
association	111,122	O	O
with	123,127	O	O
human	128,133	O	B-Process
disease	134,141	O	I-Process
.	141,142	O	O

Since	143,148	O	O
then	149,153	O	O
very	154,158	O	B-Data
large	159,164	O	I-Data
numbers	165,172	O	I-Data
of	173,175	O	O
children	176,184	O	B-Material
and	185,188	O	O
adults	189,195	O	B-Material
harbouring	196,206	O	B-Process
such	207,211	O	O
deletions	212,221	O	B-Process
have	222,226	O	O
been	227,231	O	O
described	232,241	O	O
and	242,245	O	O
it	246,248	O	O
is	249,251	O	O
clear	252,257	O	O
they	258,262	O	O
are	263,266	O	O
an	267,269	O	O
important	270,279	O	O
cause	280,285	O	O
of	286,288	O	O
human	289,294	O	B-Process
mitochondrial	295,308	O	I-Process
disease	309,316	O	I-Process
.	316,317	O	O

However	318,325	O	O
,	325,326	O	O
there	327,332	O	O
still	333,338	O	O
remain	339,345	O	O
many	346,350	O	O
important	351,360	O	O
challenges	361,371	O	O
in	372,374	O	O
relation	375,383	O	O
to	384,386	O	O
our	387,390	O	O
understanding	391,404	O	O
of	405,407	O	O
mechanisms	408,418	O	B-Process
leading	419,426	O	O
to	427,429	O	O
deletion	430,438	O	B-Process
formation	439,448	O	I-Process
and	449,452	O	O
propagation	453,464	O	B-Process
and	465,468	O	O
in	469,471	O	O
relation	472,480	O	O
to	481,483	O	O
the	484,487	O	B-Data
factors	488,495	O	I-Data
determining	496,507	O	O
the	508,511	O	B-Material
complex	512,519	O	I-Material
and	520,523	O	O
varying	524,531	O	O
relationship	532,544	O	O
between	545,552	O	O
genotype	553,561	O	B-Data
and	562,565	O	I-Data
clinical	566,574	O	I-Data
phenotype	575,584	O	I-Data
.	584,585	O	O

Although	586,594	O	O
multidisciplinary	595,612	O	B-Process
team	613,617	O	I-Process
care	618,622	O	I-Process
is	623,625	O	O
essential	626,635	O	O
and	636,639	O	O
can	640,643	O	O
improve	644,651	O	O
quality	652,659	O	B-Process
of	660,662	O	I-Process
life	663,667	O	I-Process
and	668,671	O	O
outcomes	672,680	O	B-Data
for	681,684	O	O
patients	685,693	O	B-Material
,	693,694	O	O
a	695,696	O	B-Process
definitive	697,707	O	I-Process
molecular	708,717	O	I-Process
treatment	718,727	O	I-Process
for	728,731	O	O
single	732,738	O	B-Process
mtDNA	739,744	O	I-Process
deletions	745,754	O	I-Process
remains	755,762	O	O
an	763,765	O	O
important	766,775	O	O
translational	776,789	O	B-Material
research	790,798	O	I-Material
goal	799,803	O	I-Material
.	803,804	O	O

Patients	805,813	O	B-Material
with	814,818	O	O
mtDNA	819,824	O	B-Process
deletions	825,834	O	I-Process
exhibit	835,842	O	O
a	843,844	O	B-Data
very	845,849	O	I-Data
wide	850,854	O	I-Data
range	855,860	O	I-Data
of	861,863	O	O
different	864,873	O	B-Material
clinical	874,882	O	I-Material
phenotypes	883,893	O	I-Material
with	894,898	O	O
marked	899,905	O	B-Process
variation	906,915	O	I-Process
in	916,918	O	O
age	919,922	O	B-Data
at	923,925	O	O
onset	926,931	O	B-Process
and	932,935	O	O
disease	936,943	O	B-Process
severity	944,952	O	I-Process
.	952,953	O	O

Single	954,960	O	B-Process
mtDNA	961,966	O	I-Process
deletions	967,976	O	I-Process
may	977,980	O	O
enter	981,986	O	O
into	987,991	O	O
the	992,995	O	B-Process
differential	996,1008	O	I-Process
diagnosis	1009,1018	O	I-Process
of	1019,1021	O	O
many	1022,1026	O	B-Material
different	1027,1036	O	I-Material
paediatric	1037,1047	O	I-Material
and	1048,1051	O	I-Material
adult	1052,1057	O	I-Material
presentations	1058,1071	O	I-Material
across	1072,1078	O	O
a	1079,1080	O	B-Data
wide	1081,1085	O	I-Data
range	1086,1091	O	I-Data
of	1092,1094	O	O
medical	1095,1102	O	B-Material
specialties	1103,1114	O	I-Material
,	1114,1115	O	O
although	1116,1124	O	O
neurological	1125,1137	O	B-Material
presentations	1138,1151	O	I-Material
are	1152,1155	O	O
amongst	1156,1163	O	O
the	1164,1167	O	O
most	1168,1172	O	O
common	1173,1179	O	O
.	1179,1180	O	O

In	1181,1183	O	O
this	1184,1188	O	O
review	1189,1195	O	O
,	1195,1196	O	O
we	1197,1199	O	O
examine	1200,1207	O	O
the	1208,1211	O	B-Process
molecular	1212,1221	O	I-Process
mechanisms	1222,1232	O	I-Process
underpinning	1233,1245	O	O
mtDNA	1246,1251	O	B-Process
replication	1252,1263	O	I-Process
and	1264,1267	O	O
we	1268,1270	O	O
consider	1271,1279	O	O
the	1280,1283	O	O
hypotheses	1284,1294	O	O
proposed	1295,1303	O	O
to	1304,1306	O	O
explain	1307,1314	O	O
the	1315,1318	O	B-Process
formation	1319,1328	O	I-Process
and	1329,1332	O	I-Process
propagation	1333,1344	O	I-Process
of	1345,1347	O	O
single	1348,1354	O	B-Process
large	1355,1360	O	I-Process
-	1360,1361	O	I-Process
scale	1361,1366	O	I-Process
mtDNA	1367,1372	O	I-Process
deletions	1373,1382	O	I-Process
.	1382,1383	O	O

We	1384,1386	O	O
also	1387,1391	O	O
describe	1392,1400	O	O
the	1401,1404	O	B-Data
range	1405,1410	O	I-Data
of	1411,1413	O	I-Data
clinical	1414,1422	O	I-Data
features	1423,1431	O	I-Data
associated	1432,1442	O	O
with	1443,1447	O	O
single	1448,1454	O	B-Process
mtDNA	1455,1460	O	I-Process
deletions	1461,1470	O	I-Process
,	1470,1471	O	O
outline	1472,1479	O	O
a	1480,1481	O	B-Process
molecular	1482,1491	O	I-Process
diagnostic	1492,1502	O	I-Process
approach	1503,1511	O	I-Process
and	1512,1515	O	O
discuss	1516,1523	O	O
current	1524,1531	O	O
management	1532,1542	O	O
including	1543,1552	O	O
the	1553,1556	O	B-Process
role	1557,1561	O	I-Process
of	1562,1564	O	I-Process
aerobic	1565,1572	O	I-Process
and	1573,1576	O	I-Process
resistance	1577,1587	O	I-Process
exercise	1588,1596	O	I-Process
training	1597,1605	O	I-Process
programmes	1606,1616	O	I-Process
.	1616,1617	O	O

