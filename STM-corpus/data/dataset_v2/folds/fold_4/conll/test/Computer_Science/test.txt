-DOCSTART- (S095741741101342X)

In	0,2	O	O
this	3,7	O	O
paper	8,13	O	O
,	13,14	O	O
we	15,17	O	O
set	18,21	O	O
out	22,25	O	O
to	26,28	O	O
compare	29,36	O	B-Process
several	37,44	O	B-Process
techniques	45,55	O	I-Process
that	56,60	O	O
can	61,64	O	O
be	65,67	O	O
used	68,72	O	O
in	73,75	O	O
the	76,79	O	B-Process
analysis	80,88	O	I-Process
of	89,91	O	O
imbalanced	92,102	O	B-Material
credit	103,109	O	I-Material
scoring	110,117	O	I-Material
data	118,122	O	I-Material
sets	123,127	O	I-Material
.	127,128	O	O

In	129,131	O	O
a	132,133	O	B-Process
credit	134,140	O	I-Process
scoring	141,148	O	I-Process
context	149,156	O	O
,	156,157	O	O
imbalanced	158,168	O	B-Material
data	169,173	O	I-Material
sets	174,178	O	I-Material
frequently	179,189	O	O
occur	190,195	O	O
as	196,198	O	O
the	199,202	O	B-Data
number	203,209	O	I-Data
of	210,212	O	I-Data
defaulting	213,223	O	I-Data
loans	224,229	O	I-Data
in	230,232	O	O
a	233,234	O	B-Material
portfolio	235,244	O	I-Material
is	245,247	O	O
usually	248,255	O	O
much	256,260	O	O
lower	261,266	O	O
than	267,271	O	O
the	272,275	O	B-Data
number	276,282	O	I-Data
of	283,285	O	I-Data
observations	286,298	O	I-Data
that	299,303	O	O
do	304,306	O	O
not	307,310	O	O
default	311,318	O	B-Process
.	318,319	O	O

As	320,322	O	O
well	323,327	O	O
as	328,330	O	O
using	331,336	O	O
traditional	337,348	O	B-Process
classification	349,363	O	I-Process
techniques	364,374	O	I-Process
such	375,379	O	O
as	380,382	O	O
logistic	383,391	O	B-Method
regression	392,402	O	I-Method
,	402,403	O	O
neural	404,410	O	B-Method
networks	411,419	O	I-Method
and	420,423	O	O
decision	424,432	O	B-Method
trees	433,438	O	I-Method
,	438,439	O	O
this	440,444	O	O
paper	445,450	O	O
will	451,455	O	O
also	456,460	O	O
explore	461,468	O	O
the	469,472	O	O
suitability	473,484	O	O
of	485,487	O	O
gradient	488,496	O	B-Process
boosting	497,505	O	I-Process
,	505,506	O	O
least	507,512	O	B-Process
square	513,519	O	I-Process
support	520,527	O	I-Process
vector	528,534	O	I-Process
machines	535,543	O	I-Process
and	544,547	O	O
random	548,554	O	B-Process
forests	555,562	O	I-Process
for	563,566	O	O
loan	567,571	O	B-Process
default	572,579	O	I-Process
prediction	580,590	O	I-Process
.	590,591	O	O

Five	592,596	O	B-Material
real	597,601	O	I-Material
-	601,602	O	I-Material
world	602,607	O	I-Material
credit	608,614	O	I-Material
scoring	615,622	O	I-Material
data	623,627	O	I-Material
sets	628,632	O	I-Material
are	633,636	O	O
used	637,641	O	O
to	642,644	O	O
build	645,650	O	O
classifiers	651,662	O	B-Process
and	663,666	O	O
test	667,671	O	O
their	672,677	O	B-Data
performance	678,689	O	I-Data
.	689,690	O	O

In	691,693	O	O
our	694,697	O	B-Method
experiments	698,709	O	I-Method
,	709,710	O	O
we	711,713	O	O
progressively	714,727	O	O
increase	728,736	O	O
class	737,742	O	B-Data
imbalance	743,752	O	I-Data
in	753,755	O	O
each	756,760	O	O
of	761,763	O	O
these	764,769	O	B-Material
data	770,774	O	I-Material
sets	775,779	O	I-Material
by	780,782	O	O
randomly	783,791	O	B-Process
under	792,797	O	I-Process
-	797,798	O	I-Process
sampling	798,806	O	I-Process
the	807,810	O	B-Material
minority	811,819	O	I-Material
class	820,825	O	I-Material
of	826,828	O	I-Material
defaulters	829,839	O	I-Material
,	839,840	O	O
so	841,843	O	O
as	844,846	O	O
to	847,849	O	O
identify	850,858	O	O
to	859,861	O	O
what	862,866	O	O
extent	867,873	O	O
the	874,877	O	B-Data
predictive	878,888	O	I-Data
power	889,894	O	I-Data
of	895,897	O	O
the	898,901	O	B-Method
respective	902,912	O	I-Method
techniques	913,923	O	I-Method
is	924,926	O	O
adversely	927,936	O	O
affected	937,945	O	O
.	945,946	O	O

The	947,950	O	B-Data
performance	951,962	O	I-Data
criterion	963,972	O	I-Data
chosen	973,979	O	O
to	980,982	O	O
measure	983,990	O	B-Process
this	991,995	O	O
effect	996,1002	O	O
is	1003,1005	O	O
the	1006,1009	O	B-Data
area	1010,1014	O	I-Data
under	1015,1020	O	I-Data
the	1021,1024	O	I-Data
receiver	1025,1033	O	I-Data
operating	1034,1043	O	I-Data
characteristic	1044,1058	O	I-Data
curve	1059,1064	O	I-Data
(	1065,1066	O	I-Data
AUC	1066,1069	O	I-Data
)	1069,1070	O	I-Data
;	1070,1071	O	O
Friedman	1072,1080	O	B-Method
's	1080,1082	O	I-Method
statistic	1083,1092	O	I-Method
and	1093,1096	O	I-Method
Nemenyi	1097,1104	O	I-Method
post	1105,1109	O	I-Method
hoc	1110,1113	O	I-Method
tests	1114,1119	O	I-Method
are	1120,1123	O	O
used	1124,1128	O	O
to	1129,1131	O	O
test	1132,1136	O	O
for	1137,1140	O	O
significance	1141,1153	O	B-Data
of	1154,1156	O	I-Data
AUC	1157,1160	O	I-Data
differences	1161,1172	O	I-Data
between	1173,1180	O	O
techniques	1181,1191	O	B-Method
.	1191,1192	O	O

The	1193,1196	O	O
results	1197,1204	O	O
from	1205,1209	O	O
this	1210,1214	O	B-Process
empirical	1215,1224	O	I-Process
study	1225,1230	O	I-Process
indicate	1231,1239	O	O
that	1240,1244	O	O
the	1245,1248	O	B-Method
random	1249,1255	O	I-Method
forest	1256,1262	O	I-Method
and	1263,1266	O	I-Method
gradient	1267,1275	O	I-Method
boosting	1276,1284	O	I-Method
classifiers	1285,1296	O	I-Method
perform	1297,1304	O	O
very	1305,1309	O	O
well	1310,1314	O	O
in	1315,1317	O	O
a	1318,1319	O	B-Process
credit	1320,1326	O	I-Process
scoring	1327,1334	O	I-Process
context	1335,1342	O	O
and	1343,1346	O	O
are	1347,1350	O	O
able	1351,1355	O	O
to	1356,1358	O	O
cope	1359,1363	O	O
comparatively	1364,1377	O	O
well	1378,1382	O	O
with	1383,1387	O	O
pronounced	1388,1398	O	B-Data
class	1399,1404	O	I-Data
imbalances	1405,1415	O	I-Data
in	1416,1418	O	O
these	1419,1424	O	B-Material
data	1425,1429	O	I-Material
sets	1430,1434	O	I-Material
.	1434,1435	O	O

We	1436,1438	O	O
also	1439,1443	O	O
found	1444,1449	O	O
that	1450,1454	O	O
,	1454,1455	O	O
when	1456,1460	O	O
faced	1461,1466	O	O
with	1467,1471	O	O
a	1472,1473	O	B-Data
large	1474,1479	O	I-Data
class	1480,1485	O	I-Data
imbalance	1486,1495	O	I-Data
,	1495,1496	O	O
the	1497,1500	O	B-Method
C4.5	1501,1505	O	I-Method
decision	1506,1514	O	I-Method
tree	1515,1519	O	I-Method
algorithm	1520,1529	O	I-Method
,	1529,1530	O	O
quadratic	1531,1540	O	B-Method
discriminant	1541,1553	O	I-Method
analysis	1554,1562	O	I-Method
and	1563,1566	O	O
k	1567,1568	O	B-Method
-	1568,1569	O	I-Method
nearest	1569,1576	O	I-Method
neighbours	1577,1587	O	I-Method
perform	1588,1595	O	O
significantly	1596,1609	O	B-Data
worse	1610,1615	O	I-Data
than	1616,1620	O	O
the	1621,1624	O	B-Process
best	1625,1629	O	I-Process
performing	1630,1640	O	I-Process
classifiers	1641,1652	O	I-Process
.	1652,1653	O	O

-DOCSTART- (S1389128612002496)

The	0,3	O	O
recently	4,12	O	O
proposed	13,21	O	O
TCP	22,25	O	B-Process
-	25,26	O	I-Process
targeted	26,34	O	I-Process
Low	35,38	O	I-Process
-	38,39	O	I-Process
rate	39,43	O	I-Process
Distributed	44,55	O	I-Process
Denial	56,62	O	I-Process
-	62,63	O	I-Process
of	63,65	O	I-Process
-	65,66	O	I-Process
Service	66,73	O	I-Process
(	74,75	O	I-Process
LDDoS	75,80	O	I-Process
)	80,81	O	I-Process
attacks	82,89	O	I-Process
send	90,94	O	O
fewer	95,100	O	B-Data
packets	101,108	O	B-Material
to	109,111	O	O
attack	112,118	O	B-Process
legitimate	119,129	O	B-Process
flows	130,135	O	I-Process
by	136,138	O	O
exploiting	139,149	O	O
the	150,153	O	B-Data
vulnerability	154,167	O	I-Data
in	168,170	O	O
TCP	171,174	O	B-Method
's	174,176	O	I-Method
congestion	177,187	O	I-Method
control	188,195	O	I-Method
mechanism	196,205	O	I-Method
.	205,206	O	O

They	207,211	O	O
are	212,215	O	O
difficult	216,225	O	O
to	226,228	O	O
detect	229,235	O	B-Process
while	236,241	O	O
causing	242,249	O	O
severe	250,256	O	B-Data
damage	257,263	O	I-Data
to	264,266	O	O
TCP	267,270	O	B-Material
-	270,271	O	I-Material
based	271,276	O	I-Material
applications	277,289	O	I-Material
.	289,290	O	O

Existing	291,299	O	O
approaches	300,310	O	O
can	311,314	O	O
only	315,319	O	O
detect	320,326	O	B-Process
the	327,330	O	O
presence	331,339	O	O
of	340,342	O	O
an	343,345	O	B-Process
LDDoS	346,351	O	I-Process
attack	352,358	O	I-Process
,	358,359	O	O
but	360,363	O	O
fail	364,368	O	O
to	369,371	O	O
identify	372,380	O	O
LDDoS	381,386	O	B-Process
flows	387,392	O	I-Process
.	392,393	O	O

In	394,396	O	O
this	397,401	O	O
paper	402,407	O	O
,	407,408	O	O
we	409,411	O	O
propose	412,419	O	O
a	420,421	O	B-Data
novel	422,427	O	I-Data
metric	428,434	O	I-Data
-	435,436	O	O
Congestion	437,447	O	B-Data
Participation	448,461	O	I-Data
Rate	462,466	O	I-Data
(	467,468	O	I-Data
CPR	468,471	O	I-Data
)	471,472	O	I-Data
-	473,474	O	O
and	475,478	O	O
a	479,480	O	B-Method
CPR	481,484	O	I-Method
-	484,485	O	I-Method
based	485,490	O	I-Method
approach	491,499	O	I-Method
to	500,502	O	O
detect	503,509	O	B-Process
and	510,513	O	O
filter	514,520	O	B-Process
LDDoS	521,526	O	B-Process
attacks	527,534	O	I-Process
by	535,537	O	O
their	538,543	O	O
intention	544,553	O	O
to	554,556	O	O
congest	557,564	O	B-Process
the	565,568	O	B-Material
network	569,576	O	I-Material
.	576,577	O	O

The	578,581	O	O
major	582,587	O	O
innovation	588,598	O	O
of	599,601	O	O
the	602,605	O	B-Method
CPR	606,609	O	I-Method
-	609,610	O	I-Method
base	610,614	O	I-Method
approach	615,623	O	I-Method
is	624,626	O	O
its	627,630	O	O
ability	631,638	O	O
to	639,641	O	O
identify	642,650	O	B-Process
LDDoS	651,656	O	B-Process
flows	657,662	O	I-Process
.	662,663	O	O

A	664,665	O	B-Process
flow	666,670	O	I-Process
with	671,675	O	O
a	676,677	O	B-Data
CPR	678,681	O	I-Data
higher	682,688	O	O
than	689,693	O	O
a	694,695	O	B-Data
predefined	696,706	O	I-Data
threshold	707,716	O	I-Data
is	717,719	O	O
classified	720,730	O	B-Process
as	731,733	O	O
an	734,736	O	B-Process
LDDoS	737,742	O	I-Process
flow	743,747	O	I-Process
,	747,748	O	O
and	749,752	O	O
consequently	753,765	O	O
all	766,769	O	O
of	770,772	O	O
its	773,776	O	B-Material
packets	777,784	O	I-Material
will	785,789	O	O
be	790,792	O	O
dropped	793,800	O	B-Process
.	800,801	O	O

We	802,804	O	O
analyze	805,812	O	B-Process
the	813,816	O	O
effectiveness	817,830	O	O
of	831,833	O	O
CPR	834,837	O	B-Data
theoretically	838,851	O	O
by	852,854	O	O
quantifying	855,866	O	B-Process
the	867,870	O	B-Data
average	871,878	O	I-Data
CPR	879,882	O	I-Data
difference	883,893	O	I-Data
between	894,901	O	O
normal	902,908	O	B-Process
TCP	909,912	O	I-Process
flows	913,918	O	I-Process
and	919,922	O	O
LDDoS	923,928	O	B-Process
flows	929,934	O	I-Process
and	935,938	O	O
showing	939,946	O	O
that	947,951	O	O
CPR	952,955	O	B-Data
can	956,959	O	O
differentiate	960,973	O	B-Process
them	974,978	O	O
.	978,979	O	O

We	980,982	O	O
conduct	983,990	O	B-Process
ns-2	991,995	O	B-Process
simulations	996,1007	O	I-Process
,	1007,1008	O	O
test	1009,1013	O	B-Process
-	1013,1014	O	I-Process
bed	1014,1017	O	I-Process
experiments	1018,1029	O	I-Process
,	1029,1030	O	O
and	1031,1034	O	O
Internet	1035,1043	O	B-Process
traffic	1044,1051	O	I-Process
trace	1052,1057	O	I-Process
analysis	1058,1066	O	I-Process
to	1067,1069	O	O
validate	1070,1078	O	B-Process
our	1079,1082	O	B-Data
analytical	1083,1093	O	I-Data
results	1094,1101	O	I-Data
and	1102,1105	O	O
evaluate	1106,1114	O	B-Process
the	1115,1118	O	B-Data
performance	1119,1130	O	I-Data
of	1131,1133	O	O
the	1134,1137	O	B-Method
proposed	1138,1146	O	I-Method
approach	1147,1155	O	I-Method
.	1155,1156	O	O

Experimental	1157,1169	O	O
results	1170,1177	O	O
demonstrate	1178,1189	O	O
that	1190,1194	O	O
the	1195,1198	O	B-Method
proposed	1199,1207	O	I-Method
CPR	1208,1211	O	I-Method
-	1211,1212	O	I-Method
based	1212,1217	O	I-Method
approach	1218,1226	O	I-Method
is	1227,1229	O	O
substantially	1230,1243	O	O
more	1244,1248	O	O
effective	1249,1258	O	O
compared	1259,1267	O	O
to	1268,1270	O	O
an	1271,1273	O	B-Method
existing	1274,1282	O	I-Method
Discrete	1283,1291	O	I-Method
Fourier	1292,1299	O	I-Method
Transform	1300,1309	O	I-Method
(	1310,1311	O	I-Method
DFT)-based	1311,1321	O	I-Method
approach	1322,1330	O	I-Method
-	1331,1332	O	O
one	1333,1336	O	O
of	1337,1339	O	O
the	1340,1343	O	O
most	1344,1348	O	O
efficient	1349,1358	O	O
approaches	1359,1369	O	O
in	1370,1372	O	O
detecting	1373,1382	O	B-Process
LDDoS	1383,1388	O	B-Process
attacks	1389,1396	O	I-Process
.	1396,1397	O	O

We	1398,1400	O	O
also	1401,1405	O	O
provide	1406,1413	O	O
experimental	1414,1426	O	O
guidance	1427,1435	O	O
to	1436,1438	O	O
choose	1439,1445	O	O
the	1446,1449	O	B-Data
CPR	1450,1453	O	I-Data
threshold	1454,1463	O	I-Data
in	1464,1466	O	O
practice	1467,1475	O	O
.	1475,1476	O	O

