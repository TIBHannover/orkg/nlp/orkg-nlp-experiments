-DOCSTART- (S0001870812002101)

We	0,2	O	O
prove	3,8	O	O
an	9,11	O	B-Data
integrality	12,23	O	I-Data
property	24,32	O	I-Data
of	33,35	O	O
the	36,39	O	B-Data
Chern	40,45	O	I-Data
character	46,55	O	I-Data
with	56,60	O	O
values	61,67	O	B-Data
in	68,70	O	I-Data
Chow	71,75	O	I-Data
groups	76,82	O	I-Data
.	82,83	O	O

As	84,86	O	O
a	87,88	O	O
consequence	89,100	O	O
,	100,101	O	O
we	102,104	O	O
obtain	105,111	O	O
a	112,113	O	B-Process
construction	114,126	O	I-Process
of	127,129	O	O
the	130,133	O	B-Method
p-1	134,137	O	I-Method
first	138,143	O	I-Method
homological	144,155	O	I-Method
Steenrod	156,164	O	I-Method
operations	165,175	O	I-Method
on	176,178	O	O
Chow	179,183	O	B-Data
groups	184,190	O	I-Data
modulo	191,197	O	B-Process
p	198,199	O	I-Process
and	200,203	O	I-Process
p	204,205	O	I-Process
-	205,206	O	I-Process
primary	206,213	O	I-Process
torsion	214,221	O	I-Process
,	221,222	O	O
over	223,227	O	O
an	228,230	O	B-Data
arbitrary	231,240	O	I-Data
field	241,246	O	I-Data
.	246,247	O	O

We	248,250	O	O
provide	251,258	O	O
applications	259,271	O	O
to	272,274	O	O
the	275,278	O	O
study	279,284	O	O
of	285,287	O	O
correspondences	288,303	O	B-Process
between	304,311	O	O
algebraic	312,321	O	B-Data
varieties	322,331	O	I-Data
.	331,332	O	O

-DOCSTART- (S0021869313002846)

Let	0,3	O	O
O	4,5	O	B-Data
be	6,8	O	O
a	9,10	O	B-Data
holomorphy	11,21	O	I-Data
ring	22,26	O	B-Material
in	27,29	O	O
a	30,31	O	B-Material
global	32,38	O	I-Material
field	39,44	O	I-Material
K	45,46	O	B-Data
,	46,47	O	O
and	48,51	O	O
R	52,53	O	B-Data
a	54,55	O	B-Data
classical	56,65	O	I-Data
maximal	66,73	O	I-Data
O	74,75	O	I-Data
-	75,76	O	I-Data
order	76,81	O	I-Data
in	82,84	O	O
a	85,86	O	B-Data
central	87,94	O	I-Data
simple	95,101	O	I-Data
algebra	102,109	O	I-Data
over	110,114	O	O
K.	115,117	O	B-Data

We	118,120	O	O
study	121,126	O	O
sets	127,131	O	B-Material
of	132,134	O	O
lengths	135,142	O	B-Data
of	143,145	O	O
factorizations	146,160	O	B-Process
of	161,163	O	O
cancellative	164,176	O	B-Data
elements	177,185	O	I-Data
of	186,188	O	O
R	189,190	O	B-Data
into	191,195	O	O
atoms	196,201	O	B-Data
(	202,203	O	I-Data
irreducibles	203,215	O	I-Data
)	215,216	O	I-Data
.	216,217	O	O

In	218,220	O	O
a	221,222	O	O
large	223,228	O	O
majority	229,237	O	O
of	238,240	O	O
cases	241,246	O	O
there	247,252	O	O
exists	253,259	O	O
a	260,261	O	B-Process
transfer	262,270	O	I-Process
homomorphism	271,283	O	I-Process
to	284,286	O	O
a	287,288	O	B-Data
monoid	289,295	O	I-Data
of	296,298	O	O
zero	299,303	O	B-Data
-	303,304	O	I-Data
sum	304,307	O	I-Data
sequences	308,317	O	B-Material
over	318,322	O	O
a	323,324	O	B-Material
ray	325,328	O	I-Material
class	329,334	O	I-Material
group	335,340	O	I-Material
of	341,343	O	O
O	344,345	O	B-Data
,	345,346	O	O
which	347,352	O	O
implies	353,360	O	O
that	361,365	O	O
all	366,369	O	B-Data
the	370,373	O	I-Data
structural	374,384	O	I-Data
finiteness	385,395	O	I-Data
results	396,403	O	I-Data
for	404,407	O	O
sets	408,412	O	B-Material
of	413,415	O	O
lengths	416,423	O	B-Data
-	423,424	O	O
valid	424,429	O	O
for	430,433	O	O
commutative	434,445	O	B-Data
Krull	446,451	O	I-Data
monoids	452,459	O	I-Data
with	460,464	O	O
finite	465,471	O	B-Material
class	472,477	O	I-Material
group	478,483	O	I-Material
-	483,484	O	O
hold	484,488	O	O
also	489,493	O	O
true	494,498	O	O
for	499,502	O	O
R.	503,505	O	B-Data

If	506,508	O	O
O	509,510	O	B-Data
is	511,513	O	O
the	514,517	O	B-Material
ring	518,522	O	I-Material
of	523,525	O	O
algebraic	526,535	O	B-Data
integers	536,544	O	I-Data
of	545,547	O	O
a	548,549	O	B-Material
number	550,556	O	I-Material
field	557,562	O	I-Material
K	563,564	O	B-Data
,	564,565	O	O
we	566,568	O	O
prove	569,574	O	O
that	575,579	O	O
in	580,582	O	O
the	583,586	O	B-Material
remaining	587,596	O	I-Material
cases	597,602	O	I-Material
no	603,605	O	O
such	606,610	O	O
transfer	611,619	O	B-Process
homomorphism	620,632	O	I-Process
can	633,636	O	O
exist	637,642	O	O
and	643,646	O	O
that	647,651	O	O
several	652,659	O	B-Material
invariants	660,670	O	I-Material
dealing	671,678	O	O
with	679,683	O	O
sets	684,688	O	B-Material
of	689,691	O	O
lengths	692,699	O	B-Data
are	700,703	O	O
infinite	704,712	O	B-Data
.	712,713	O	O

