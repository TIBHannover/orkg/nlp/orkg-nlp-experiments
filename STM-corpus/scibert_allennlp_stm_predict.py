import subprocess
import os
import sys
from eval_scienceie import calculateMeasures
import json
import re
from gpu_executor import GPUExecutor
from span_based_accuracy import span_accuracy
from token_based_evaluation import calc_classification_metrics_from_file

# ADAPT: provide the folder with the best models for
run = "stm_v2_best_models"


# ADAPT: provide the GPUs to be used. For each GPU a concurrent prediction is executed.
#gpus = [0, 1, 2, 3, 4, 5, 6, 7]
gpus = [0]

###################
dataset = "dataset_v2"
labels = ['Data', 'Material', 'Method', 'Process', 'O']
domains = ["overall", "Agriculture", "Astronomy", "Biology", "Chemistry", "Computer_Science", "Earth_Science", "Engineering", "Materials_Science", "Mathematics", "Medicine"]
run_path = f'results/{run}'


def call_predict(gpu, output_path, fold, split, domain):
    predict_test_cmd = ' '.join(['python',
                                 'predict_semeval2017_with_allennlp.py',
                                 f'--experiment_dir="{output_path}"',
                                 f'--conll_file="data/dataset_v1/folds/fold_{fold}/conll/{split}/{domain}/{split}.txt"',
                                 f'--output_dir="{predict_path(output_path, split, domain)}"',
                                 '--gpu'])
    env = {
        'CUDA_VISIBLE_DEVICES': str(gpu)
    }
    print(env)
    print(predict_test_cmd)
    completed = subprocess.run(predict_test_cmd, shell=True, env=env)
    print(f'returncode: {completed.returncode}')


def standoff_path(fold, split, domain):
    return f"data/{dataset}/folds/fold_{fold}/standoff/{split}/{domain}"


def predict_path(output_path, split, domain):
    return f"{output_path}/{split}_{domain}_predicted"


def calculate_metrics(output_path, fold, domain, dropout, lr, lstm_hs, dev_f1_ap, test_f1_ap):
    # evaluate predictions for dev and test set with scienceie eval script
    test_metrics = calculateMeasures(standoff_path(fold, "test", domain), predict_path(output_path, "test", domain),
                                     remove_anno="rel")
    dev_metrics = calculateMeasures(standoff_path(fold, "dev", domain), predict_path(output_path, "dev", domain),
                                    remove_anno="rel")

    test_metrics_span = calculateMeasures(standoff_path(fold, "test", domain),
                                          predict_path(output_path, "test", domain),
                                          remove_anno="types")
    dev_metrics_span = calculateMeasures(standoff_path(fold, "dev", domain), predict_path(output_path, "dev", domain),
                                         remove_anno="types")

    dev_span_accuracy = span_accuracy(standoff_path(fold, "dev", domain), predict_path(output_path, "dev", domain))
    test_span_accuracy = span_accuracy(standoff_path(fold, "test", domain), predict_path(output_path, "test", domain))

    test_tokenbased_metrics = calc_classification_metrics_from_file(
        os.path.join(predict_path(output_path, "test", domain),
                     "token_gold_predicted_labels.csv"), labels=labels)

    dev_tokenbased_metrics = calc_classification_metrics_from_file(
        os.path.join(predict_path(output_path, "dev", domain),
                     "token_gold_predicted_labels.csv"), labels=labels)

    result = {
        "fold": fold,
        "domain": domain,
        "dropout": dropout,
        "lr": lr,
        "lstm_hs": lstm_hs,
        "dev_f1_allennlp": dev_f1_ap,
        "test_f1_allennlp": test_f1_ap,
        "test": test_metrics,
        "dev": dev_metrics,
        "test_span": test_metrics_span,
        "dev_span": dev_metrics_span,
        "test_token_based": test_tokenbased_metrics,
        "dev_token_based": dev_tokenbased_metrics,
        "dev_span_accuracy": dev_span_accuracy,
        "test_span_accuracy": test_span_accuracy
    }
    return result


def call_predictions(metrics, output_path, fold, domain, dropout, lr, lstm_hs, dev_f1_ap, test_f1_ap):
    def c(gpu):

        print(f'predicting for {domain} {output_path}')

        call_predict(gpu, output_path, fold, "test", domain)
        call_predict(gpu, output_path, fold, "dev", domain)

        run_metrics = calculate_metrics(output_path, fold, domain, dropout, lr, lstm_hs, dev_f1_ap, test_f1_ap)
        metrics.append(run_metrics)

    return c




def perform_predictions():   
    for d_path in list(os.listdir(run_path)):
        # collect for each output_path the metrics and write them at the end of the execution
        output_path_metrics = dict()
        executor = GPUExecutor(gpus=gpus)
        print(d_path)
        for output_path in list(os.listdir(os.path.join(run_path, d_path))):
            m = re.match(".*_fold_(\\d+).*", output_path)
            if not m:
                print(f'Skipping {output_path}')
                continue

            fold = m.group(1)

            output_path = os.path.join(run_path, d_path, output_path)
            
            print(output_path)

            if not os.path.exists(f'{output_path}/metrics.json'):
                print("Skipping as no metrics.json: " + output_path)
                continue

            with open(f'{output_path}/metrics.json', "r", encoding="utf-8") as am:
                allen_metrics = json.load(am)

            with open(f'{output_path}/config.json', "r", encoding="utf-8") as params_f:
                params = json.load(params_f)

            lr = params["trainer"]["optimizer"]["lr"]
            dropout = params["model"]["encoder"]["dropout"]
            lstm_hs = params["model"]["encoder"]["hidden_size"]
            dev_f1_ap = allen_metrics["best_validation_f1-measure-overall"]
            test_f1_ap = allen_metrics["test_f1-measure-overall"]

            metrics = []
            output_path_metrics[output_path] = metrics
            for domain in domains:
                executor.submit(
                    call_predictions(metrics, output_path, fold, domain, dropout, lr, lstm_hs, dev_f1_ap, test_f1_ap))

        # wait until all predictions have been written
        executor.shutdown()

        # write the results into metrics_scienceie2017.json for each fold and combination
        for op, m in output_path_metrics.items():
            with open(f'{op}/metrics_scienceie2017.json', "w", encoding="utf-8") as m_f:
                json.dump(m, m_f)


        print("finished")


perform_predictions()

