import glob
import os

from standoffreader import parse_entities


def load_spans(path):
    spans = set()
    for ann in glob.glob(f'{path}/*.ann'):
        id = os.path.basename(ann)
        with open(ann, "r", encoding="utf--8") as ann_f:
            ann_txt = ann_f.read()
        entities = parse_entities(ann_txt)
        for e in entities:
            spans.add((id, e.start, e.end))
    return spans


def span_accuracy(gold_folder, predicted_folder):
    gold_spans = load_spans(gold_folder)
    predicted_spans = load_spans(predicted_folder)
    return calc_span_accuracy(gold_spans, predicted_spans)


def calc_span_accuracy(gold_spans, predicted_spans):
    # accuracy = (tp + tn) / (tp+fp+fn+tn)
    # where
    # tp = correct predicted spans
    # fp = incorrect spans that are not in gold data
    # fn = missed spans in predictions
    # tn = 0
    tp = gold_spans.intersection(predicted_spans)
    fp = predicted_spans.difference(gold_spans)
    fn = gold_spans.difference(predicted_spans)

    denominator = len(tp) + len(fp) + len(fn)
    if denominator == 0:
        return 0.0
    accuracy = len(tp) / denominator

    return accuracy

