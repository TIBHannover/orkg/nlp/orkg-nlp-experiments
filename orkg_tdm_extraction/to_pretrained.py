import os
import torch

from transformers import XLNetForSequenceClassification

CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))
MODEL_PATH = os.path.join(CURRENT_DIR, 'models', 'orkgnlp-tdm-extraction-xlnet.pt')
PRETRAINED_MODEL_PATH = os.path.join(CURRENT_DIR, 'output', 'orkgnlp-tdm-extraction-xlnet')


def main():
    model = XLNetForSequenceClassification.from_pretrained('xlnet-base-cased')
    model.load_state_dict(torch.load(MODEL_PATH, map_location=torch.device('cpu')))

    model.save_pretrained(PRETRAINED_MODEL_PATH)


if __name__ == '__main__':
    main()

